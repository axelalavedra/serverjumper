var exports = module.exports = {};

var players = {};

var GAME_RUNNING = 1;
var GAME_OVER = 4;
var GAME_CHALLENGEDEAD = 6;
var GAME_CHALLENGEWIN = 7;
var GAME_WAITINGPLAYER = 5;
var GAME_CONNECTIONERROR = 8;

exports.addPlayer = function(id, name){
	players[id] = { playerid:id, playerstate:"ALIVE", height:0, gamestate:GAME_WAITINGPLAYER } ;
}

exports.isReadyToPlay = function(){
	var result = [];
	for (var i in players) {
		result.push(players[i]);
	}

	if (result.length > 1) {
		for(var i in players){
			players[i].gamestate = GAME_RUNNING;
		}
		return true;
	}
	return false;
}

exports.update = function(id, height, playerstate)
{
	updatePlayer(id, height, playerstate);
	for(var i in players){
		updateGameState(players[i].playerid);
	}
}

function updatePlayer(id, _height, _playerstate){
	players[id].height = _height;
	players[id].playerstate = _playerstate;
}

function updateGameState(id){
	
	var myplayerstate, myheight;
	var mygamestate = 1;
	var challengerplstate, challengerheight, challengergmstate;

	
	for(var i in players){
		if(players[i].playerid != id){
			challengerplstate = players[i].playerstate;
			challengerheight = players[i].height;
			challengergmstate = players[i].gamestate;
		}
		else{
			myplayerstate = players[i].playerstate;
			myheight = players[i].height;
		}
	}
	
	if(myplayerstate == "DEAD"){
		if(parseFloat(myheight) < parseFloat(challengerheight)){
			mygamestate = GAME_OVER;
		}
		else {
			if(challengerplstate == "ALIVE") mygamestate = GAME_CHALLENGEDEAD;
			else mygamestate = GAME_CHALLENGEWIN;
		}
	}
	else{
		if(challengerplstate == "DEAD"){
			if(parseFloat(myheight) < parseFloat(challengerheight)) mygamestate = GAME_RUNNING;
			else mygamestate = GAME_CHALLENGEWIN;
		}
	}	
	if(challengergmstate == GAME_CHALLENGEWIN) mygamestate = GAME_OVER;
	
	players[id].gamestate = mygamestate;
}

exports.setGameState = function(id, _gamestate){
	players[id].gamestate = _gamestate;
}

exports.getGameState = function(id){
	var result = players[id].gamestate;
	return result;
}

exports.finishGame = function(id){
	for(var i in players){
		if(players[i].playerid != id) players[i].gamestate = GAME_OVER;
		else players[i].gamestate = GAME_CHALLENGEWIN;
	}
}
exports.resetPlayers = function(){
	players = {};
}

exports.isFinished = function(){
	for(var i in players){
		if(players[i].gamestate == GAME_OVER) return true;
	}
}
