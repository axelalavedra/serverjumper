var connectedClientsList = function(){

	this.connectedClients = {};

	this.setChannelToClient = function(id_socket, subscriber, channel) {
		this.connectedClients[id_socket].channel = channel;
		this.connectedClients[id_socket].subscriber = subscriber;
	};

	this.getSubscriberSockets = function(id_subscriber) {
		var result = [];
		for (var i in this.connectedClients) {
			var subscribers = this.connectedClients[i].subscribers;
			if ( subscribers.indexOf(id_subscriber)!==false ) result.push(this.connectedClients[i].socket);
		}
		return result;
	};

	this.getSubscriberDetail = function(id_subscriber) {
		var result = [];
		for (var i in this.connectedClients) {
			var subscribers = this.connectedClients[i].subscribers;
			if ( subscribers.indexOf(id_subscriber)!=-1) {
				result.push( { id_socket: i,  channel:  this.connectedClients[i].channel });
			}
		}
		return result;
	};

	this.getSubscribers = function(channel) {
		var array = [];
		for (var i in this.connectedClients) {
			var subscribers = this.connectedClients[i].subscribers;
			if (!channel || this.connectedClients[i].channel == channel) array = array.concat(subscribers);
		}
		var result = array.filter(function(elem, pos, self) {
		    return self.indexOf(elem) == pos;
		}); // getting rid of duplicates
		return result;
	};

	this.getSubscribersDetail = function(channel) {
		var result = [];
		for (var i in this.connectedClients) {
			var subscribers = this.connectedClients[i].subscribers;
			if (!channel || this.connectedClients[i].channel == channel) result = result.concat( { id_socket: i,  channel:  this.connectedClients[i].channel, subscribers: subscribers });
		}
		return result;
	};

	this.getChannels = function() {
		var result = [];
		for (var i in this.connectedClients) {
			if (this.connectedClients[i].channel) {
				if (result.indexOf(this.connectedClients[i].channel) == -1) result.push(this.connectedClients[i].channel);
			}
		}
		return result;
	};

	this.addSubscriber = function(id_socket, id_subscriber) {
		if ( this.connectedClients[id_socket] && this.connectedClients[id_socket].subscribers.indexOf(id_subscriber)==-1 ) {
			this.connectedClients[id_socket].subscribers.push(id_subscriber);
			return true;
		} else {
			return false;
		}
	};

	this.deleteSubscriber = function(id_socket, id_subscriber) {

		if (this.connectedClients[id_socket]) {
			var index = this.connectedClients[id_socket].subscribers.indexOf(id_subscriber);
			if ( index!==false)  {
				this.connectedClients[id_socket].subscribers.splice(index,1);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}

	};

	this.addClient = function(channel, socket, id_socket){
		this.connectedClients[id_socket]  = { channel: channel, socket:socket, subscribers: [], id:id_socket } ;
	};

	this.getSocket = function(id_socket) {
			return this.connectedClients[id_socket];
	};

	this.send = function(id_socket, data) {
		this.connectedClients[id_socket].socket.write(data, function() {
			//
		});
	};

	this.sendAll = function(data, callback, params) {
		var _t = Object.keys(this.connectedClients) + 1, _callback = function(r) {
			_t--;
			if (_t<=0) {
				if (typeof callback == "function") callback();
			}
		}		
		for (var i in this.connectedClients) {
				var ignore = false;
				if (params && params.exceptions) {
					ignore = params.exceptions[0] == i;
				}
				if (!ignore) {
					console.log("Sending from "+params.exceptions[0]+" to socket " + i + " data:", data);
					this.connectedClients[i].socket.write(data, _callback);
				}
		}
		_callback();
	};
	this.getSockets = function(channel) {
		if (!channel) {
			return this.connectedClients;
		} else {
			var result = [];
			for (var i in this.connectedClients) {
				if (this.connectedClients[i].channel==channel) result.push(this.connectedClients[i]);
			}
			return result;
		}
	};

	this.removeSocket = function(id_socket){
		delete this.connectedClients[id_socket];
	};
};

module.exports = connectedClientsList;
